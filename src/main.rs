use display::display_alert_window;

mod activitywatch;
mod display;
mod settings;
mod time;

fn total_not_afk_hours(events: Vec<activitywatch::Event>) -> f64 {
    let mut total = 0.0;
    for event in events {
        if event.data.status == "not-afk" {
            total += event.duration;
        }
    }
    total / (60.0 * 60.0)
}

fn hours_today() -> f64 {
    let since = time::since_today();
    let events = activitywatch::get_afk_bucket_events(since).expect("could not get");
    total_not_afk_hours(events)
}

fn hours_this_week() -> f64 {
    let since = time::since_this_week();
    let events = activitywatch::get_afk_bucket_events(since).expect("could not get");
    total_not_afk_hours(events)
}

fn main() {
    let cfg = settings::get_settings();
    let daily_hours = hours_today();
    println!("{:.2} hours at the keyboard today", daily_hours);
    let weekly_hours = hours_this_week();
    println!("{:.2} hours at keyboard this week", weekly_hours);

    let mut alert_message = String::new();
    if weekly_hours >= cfg.min_hours_per_week_to_alert {
        alert_message.push_str(
            format!(
                "You have been at the keyboard {:.2} hours this week!\n",
                weekly_hours
            )
            .as_str(),
        );
    }
    if daily_hours >= cfg.min_hours_per_day_to_alert {
        alert_message.push_str(
            format!(
                "You have been at the keyboard {:.2} hours today!\n",
                daily_hours
            )
            .as_str(),
        )
    }
    if !alert_message.is_empty() {
        display_alert_window(alert_message);
    }
}
