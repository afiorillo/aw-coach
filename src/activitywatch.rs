use chrono::prelude::*;
use hostname;
use reqwest::Error;
use serde::{Deserialize, Serialize};
use urlencoding::encode;

// see https://github.com/ActivityWatch/aw-client/blob/master/aw_client/cli.py#L42-L51
// for reference
const DEFAULT_AW_SERVER_HOST: &str = "http://127.0.0.1";
const DEFAULT_AW_SERVER_PORT: i32 = 5600;

#[derive(Serialize, Deserialize, Debug)]
pub struct EventData {
    pub status: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Event {
    id: i32,
    timestamp: DateTime<Utc>,
    pub duration: f64,
    pub data: EventData,
}

pub fn get_afk_bucket_id() -> String {
    let name = hostname::get().expect("could not get hostname");
    let displayable = name.to_str().expect("could not convert to string");
    String::from(format!("aw-watcher-afk_{}", displayable))
}

pub fn get_afk_bucket_events(since: DateTime<Utc>) -> Result<Vec<Event>, Error> {
    let bucket_id = get_afk_bucket_id();
    // see
    // https://github.com/ActivityWatch/aw-client/blob/master/aw_client/client.py#L160
    let endpoint = format!("/api/0/buckets/{}/events", bucket_id.as_str());
    let full_url = format!(
        "{}:{}{}?start={}",
        DEFAULT_AW_SERVER_HOST,
        DEFAULT_AW_SERVER_PORT,
        endpoint,
        encode(since.to_rfc3339().as_str())
    );
    // println!("calling {}", full_url);
    let resp = reqwest::blocking::get(full_url)?.json::<Vec<Event>>()?;
    // println!("{:#?}", resp);
    Ok(resp)
}
