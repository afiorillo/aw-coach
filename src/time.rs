use chrono::{prelude::*, Duration};

// when we will call the day anew
const DAY_STARTS_ON_HOUR: u32 = 0;
// when we will call the week started
const WEEK_STARTS_ON_DAY: Weekday = Weekday::Mon;

pub fn since_today() -> DateTime<Utc> {
    let now = Local::now();
    let today_local =
        Local
            .ymd(now.year(), now.month(), now.day())
            .and_hms(DAY_STARTS_ON_HOUR, 0, 0);
    // println!("{:?}", today_local);
    today_local.with_timezone(&Utc)
}

pub fn since_this_week() -> DateTime<Utc> {
    let mut today = since_today();
    while today.weekday() != WEEK_STARTS_ON_DAY {
        today = today - Duration::days(1);
    }
    today
}
