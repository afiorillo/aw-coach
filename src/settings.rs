use config::Config;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

#[derive(Deserialize, Serialize, Debug)]
pub struct Settings {
    pub min_hours_per_day_to_alert: f64,
    pub min_hours_per_week_to_alert: f64,
}

fn get_config_filepath() -> PathBuf {
    match home::home_dir() {
        Some(pathbuf) => pathbuf.join(Path::new(".config/aw-coach/settings.toml")),
        None => Path::new("settings.toml").to_path_buf(),
    }
}

pub fn get_settings() -> Settings {
    let builder = Config::builder()
        .set_default("min_hours_per_week_to_alert", 38.0)
        .expect("unable to set default")
        .set_default("min_hours_per_day_to_alert", 8)
        .expect("unable to set default")
        // the configuration file should be optional, but this only works if all of the values have defaults!
        // otherwise, if a value is needed by the settings struct, we panic at deserialization time
        .add_source(config::File::from(get_config_filepath()).required(false));

    builder
        .build()
        .expect("unable to build config stack")
        .try_deserialize::<Settings>()
        .expect("unable to build settings")
}
