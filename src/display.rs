use notify_rust::{Hint, Notification};

pub fn display_alert_window(message: String) -> Option<()> {
    Notification::new()
        .summary("aw-coach")
        .body(message.as_str())
        .icon("warning")
        .appname("aw-coach")
        .timeout(0) // this requires the notifcation to be acked
        .show()
        .expect("could not display notification");
    Some(())
}
