# aw-coach

A coach for your ActivityWatch stats.

## Setup

This project uses `cargo` and `rust`, so have those installed.

```bash
$ rustc --version
rustc 1.62.1 (e092d0b6b 2022-07-16)
$ cargo --version
cargo 1.62.1 (a748cf5a3 2022-06-08)
```

To build, use your usual cargo friends:

```bash
# build and run now
$ cargo run
# run tests, when they exist
$ cargo test
# package for distribution
$ cargo build --release
# install on the current machine
$ cargo install --path .
```

### Non-Rust Dependencies

This project uses `libnotify` to display alerts to the user.
Follow instructions details from [notify-rust](https://github.com/hoodie/notify-rust) if your operating system needs additional instructions.

## Configuration

An optional configuration file can be placed in `$HOME/.config/aw-coach/settings.toml` that contains custom thresholds for alerting.
To create such a file:

```bash
$ mkdir -p ~/.config/aw-coach
$ cat >> ~/.config/aw-coach/settings.toml<< EOF
# more than this per week and you will get an alert window
min_hours_per_week_to_alert = 40
# more than this on any given day and you will get an alert window
min_hours_per_day_to_alert = 8
EOF
```

### Cron helper

An easy way to run aw-coach periodically is with crontab.
Simply add a crontab line like this to run every hour on the hour

```crontab
0 * * * * DISPLAY=:0 aw-coach | logger -t aw-coach
```
